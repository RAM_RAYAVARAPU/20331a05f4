import java.util.*;
public class EvenOddJava {
void EvenOdd(int a) {
        if (a % 2 == 0)// if block
            System.out.println("Number is even");
        else// else block
            System.out.println("Number is odd");
    }
    public static void main(String[] args){
        System.out.println("Enter an integer:");
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();// integer input is stored in a
        EvenOddJava obj = new EvenOddJava();
        obj.EvenOdd(a);
    
}

    
}


